import React from 'react';

// Componente: Bloco isolado de HTML, CSS e JS, que nao interfere no restante da aplicação;
// Propriedades: Informaçoes que um componente Pai para o componente Filho;
// Estado: Informações mantidas pelo componente (Lembrar: imutabilidade);

// quando precisar criar uma <div></div>, mas isso for quebrar a estilizacao, basta somente deixar <></> que o REACT reconhece.

import './global.css';
import './App.css';
import './Sidebar.css';
import './Main.css';

function App() {
  return (
    <div id="app">
      <aside>
        <strong>Cadastrar</strong>
        <form>
          <div className="input-block">
            <label htmlFor="github_username">Usuário do Github</label>
            <input name="github_username" id="github_username" required/>
          </div>
          
          <div className="input-block">
            <label htmlFor="techs">Tecnologias</label>
            <input name="techs" id="techs" required/>
          </div>

        <div className="input-group">
          <div className="input-block">
            <label htmlFor="latitude">Latitude</label>
            <input name="latitude" id="latitude" required/>
          </div>
          <div className="input-block">
            <label htmlFor="longitude">Longitude</label>
            <input name="longitude" id="longitude" required/>
          </div>
        </div>
          
          <button type="submit">Salvar</button>
        </form>
      </aside>

      <main>
        <ul>
          <li className="dev-item">
            <header>
            <img src="https://t3.ftcdn.net/jpg/02/12/43/28/240_F_212432820_Zf6CaVMwOXFIylDOEDqNqzURaYa7CHHc.jpg" alt="Marcos Bruno"/>
              <div className="user-info">
                <strong>Marcos Bruno</strong>
                <span>Node.js, React</span>
              </div>
            </header>
            <p>Alguma coisa escrita como bio</p>
            <a href="https://www.google.com.br">Acessar perfil no github</a>
          </li>

          <li className="dev-item">
            <header>
            <img src="https://t3.ftcdn.net/jpg/02/12/43/28/240_F_212432820_Zf6CaVMwOXFIylDOEDqNqzURaYa7CHHc.jpg" alt="Marcos Bruno"/>
              <div className="user-info">
                <strong>Marcos Bruno</strong>
                <span>Node.js, React</span>
              </div>
            </header>
            <p>Alguma coisa escrita como bio</p>
            <a href="https://www.google.com.br">Acessar perfil no github</a>
          </li>

          <li className="dev-item">
            <header>
            <img src="https://t3.ftcdn.net/jpg/02/12/43/28/240_F_212432820_Zf6CaVMwOXFIylDOEDqNqzURaYa7CHHc.jpg" alt="Marcos Bruno"/>
              <div className="user-info">
                <strong>Marcos Bruno</strong>
                <span>Node.js, React</span>
              </div>
            </header>
            <p>Alguma coisa escrita como bio</p>
            <a href="https://www.google.com.br">Acessar perfil no github</a>
          </li>

          <li className="dev-item">
            <header>
            <img src="https://t3.ftcdn.net/jpg/02/12/43/28/240_F_212432820_Zf6CaVMwOXFIylDOEDqNqzURaYa7CHHc.jpg" alt="Marcos Bruno"/>
              <div className="user-info">
                <strong>Marcos Bruno</strong>
                <span>Node.js, React</span>
              </div>
            </header>
            <p>Alguma coisa escrita como bio</p>
            <a href="https://www.google.com.br">Acessar perfil no github</a>
          </li>
        </ul>
      </main>
    </div>
  );
}

export default App;
